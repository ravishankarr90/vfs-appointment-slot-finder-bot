## Steps to set up

1. Check out this project in your system.
2. Create an account in VFS Global , https://visa.vfsglobal.com/ind/en/deu/login
3. If you need SMS notification then, create an account in Twilio , https://www.twilio.com/try-twilio to send SMS notification of appointment slots.
4. If you need to email notification then, create an email account in Gmail and follow the process mentioned in the video, https://www.youtube.com/watch?v=g_j6ILT-X0k
5. Install Python 3 in your system from , https://www.python.org/downloads
6. Ensure Python 3 is properly installed in your by opening terminal and typing ,

In Windows

```
python --version
```

In Linux

```
python3 --version
```

7. Download Mozilla geckodriver from, https://github.com/mozilla/geckodriver/releases/tag/v0.31.0
8. Update the config.ini file inside the project folder with your credentials and account details also enable the notifications you need ( By default both Email and SMS notifications are disabled).
9. Run the below command in the the terminal of project folder path,

```
pip install -r requirements.txt
```

10. By default the appointment slot check duration is set to 120 ( seconds ), you can change the interval according to your need.
11. To start the execution of the script run the below command in the terminal of your project folder path,

In Windows

```
python vfs-appointment-check-bot.py
```

You can also run the script by providing the arguments directly like shown in the example below,

```
python vfs-appointment-check-bot.py "Cochin - Visa Application Centre" "Schengen Visa (stay of max. 90 days or less)" "business"
```

In Linux

```
python3 vfs-appointment-check-bot.py
```

You can also run the script by providing the arguments directly like shown in the example below,

```
python3 vfs-appointment-check-bot.py "Cochin - Visa Application Centre" "Schengen Visa (stay of max. 90 days or less)" "business"
```

12. The script will prompt you to enter visa center, visa category and subcategory of your choice.
13. If there is any available slot then an SMS will be sent to your specified number.
14. The script will check for a slot at regular intervals specified in the config.ini
