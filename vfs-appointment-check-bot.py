from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from decouple import config
from heyoo import WhatsApp
import time
import datetime

# For sending a Text messages
messenger = WhatsApp(config("WHATSAPP_ACCESS_KEY"),phone_number_id = config("WHATSAPP_PHONE_NO_ID"))

def get_appointment_date(browser , visa_centre, visa_category, visa_subcategory):
    print("Getting appointment date: Visa Centre: {}, Category: {}, Sub-Category: {}".format(visa_centre, visa_category, visa_subcategory)) 
    # select from drop down
    time.sleep(5)
    _visa_centre_dropdown = browser.find_element(By.XPATH,
        "//mat-form-field/div/div/div[3]"
    )
    _visa_centre_dropdown.click()
    time.sleep(2)

    try:
        _visa_centre = browser.find_element(By.XPATH,
            "//mat-option[starts-with(@id,'mat-option-')]/span[contains(text(), '{}')]".format(visa_centre)
        )
    except NoSuchElementException:
        raise Exception("Visa centre not found: {}".format(visa_centre))
    
    print("VFS Centre: " + _visa_centre.text)
    browser.execute_script("arguments[0].click();", _visa_centre)
    time.sleep(5)
    
    _category_dropdown = browser.find_element(By.XPATH,
        "//div[@id='mat-select-value-3']"
    )
    _category_dropdown.click()
    time.sleep(5)
    
    try:
        _category = browser.find_element(By.XPATH,
            "//mat-option[starts-with(@id,'mat-option-')]/span[contains(text(), '{}')]".format(visa_category)
        )
    except NoSuchElementException:
        raise Exception("Category not found: {}".format(visa_category))
    
    print("Category: " + _category.text)
    browser.execute_script("arguments[0].click();", _category)
    time.sleep(5)
    
    _subcategory_dropdown = browser.find_element(By.XPATH,
        "//div[@id='mat-select-value-5']"
    )
    
    browser.execute_script("arguments[0].click();", _subcategory_dropdown)
    time.sleep(5)
    
    try:
        _subcategory = browser.find_element(By.XPATH,
            "//mat-option[starts-with(@id,'mat-option-')]/span[contains(text(), '{}')]".format(visa_subcategory)
        )
    except NoSuchElementException:
        raise Exception("Sub-category not found: {}".format(visa_subcategory))
    
    browser.execute_script("arguments[0].click();", _subcategory)
    print("Sub-Cat: " + _subcategory.text)
    time.sleep(5)

    # read contents of the text box
    return browser.find_element(By.XPATH,"//div[4]/div")

def send_whatsapp_msg(notification_body):
    messenger.send_message(notification_body, '919995830739')

def check_appointment_slot(visa_centre, visa_category, visa_subcategory):

    options = Options()
    options.headless = True
    browser = webdriver.Firefox(options=options)
    browser.get("https://visa.vfsglobal.com/ind/en/deu/login")
    try:

        time.sleep(10)
        print("---Login page is ready")

        email = browser.find_element(By.ID ,"mat-input-0")
        password = browser.find_element(By.ID ,"mat-input-1")

        email.send_keys(config("VFS_EMAIL"))
        password.send_keys(config("VFS_PASSWORD"))
        javas = "document.getElementsByClassName('btn-brand-orange')[0].click();"
        browser.execute_script(javas)

        time.sleep(10)
        print("---Home page is ready")

        javas = "document.getElementsByClassName('btn mat-btn-lg btn-brand-orange')[0].click();"
        browser.execute_script(javas)

        message = get_appointment_date(browser , visa_centre, visa_category, visa_subcategory)
        #message = get_appointment_date(browser,"Cochin - Visa Application Centre" ,"Schengen Visa (stay of max. 90 days or less)","business" )

        if len(message.text) != 0 and message.text != "No appointment slots are currently available" and message.text != "Currently No slots are available for selected category, please confirm waitlist\nTerms and Conditions":
            print("Appointment slots available: {}".format(message.text))
            ts = time.time()
            st = datetime.datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S")
            notification_body = "{} checked at {}".format(message.text, st) # Message to notify
            print(notification_body)
            #send_email_notification(config,notification_body)
            #send_sms_notification(config, notification_body)
            send_whatsapp_msg(notification_body)
        else:
            print("No slots available")

    except TimeoutException:
        print("###Login Time out###")

    #browser.close()
    #browser.quit()
    return browser

def countdown(t):
    while t > -1:
        timer = 'Retry after {:02d} seconds'.format(t)
        print(timer, end="\r")
        time.sleep(1)
        t -= 1

def init():
    
    visa_category = "Schengen Visa (stay of max. 90 days or less)"
    visa_subcategory = "business"
    visa_centers = ["Cochin - Visa Application Centre" ,"Chennai - Visa Application Centre","Bangalore - Visa Application Centre","Panaji - Visa Application Centre","Puducherry - Germany Visa Application Centre"]

    count  = 1
    interval = config('INTERVAL')

    while True:
        try:
            print("Running VFS Appointment Bot: Attempt#{}".format(count))
            for index, visa_centre in enumerate(visa_centers):
                browser = check_appointment_slot(visa_centre, visa_category, visa_subcategory)
                if index == (len(visa_centers) - 1):
                    browser.close()
                    browser.quit()
                else:
                    time.sleep(5)
            print("Sleeping for {} seconds".format(interval))
            countdown(int(interval))
        except Exception as e:
            countdown(int(60))
            pass
        print("\n")
        count += 1



    
if __name__ == "__main__":
    init()